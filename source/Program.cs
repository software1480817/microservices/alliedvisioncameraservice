﻿/*
 * Erstellt mit SharpDevelop.
 * Benutzer: rwolf
 * Datum: 06.12.2016
 * Zeit: 09:34
 * 
 * Release notes:
 * 	Version 2.0: 
 * 		- frame aquisition mode changed. Softwaretrigger works much faster than old method via AquireSingleImage. Starting image aquisition and capturing on camera connect.
 * 		- mayor changes in SendCameraSnapshot
 * 	Version 3.0
 * 	    - added error handling
 * 	    - added both snap shot modes: SW Trigger and 
 * 	    - new in connect camera
 * 	        1.set default settings at first
 * 	        2.adjust package size by HW 	    
 * 	    - added save default settings function if not been saved
 * 	    - added save settings on connect if request
 * 	    - added save setting on error
 * 	    - added reverse x and y commands
 * 	    
 */

using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using AVT.VmbAPINET;


namespace ConnectAlliedVisionCameras
{
    class ConnectAlliedVisionCameras
    {
        /// <summary>
        /// consts
        /// </summary>
        private const int cTIMEOUT_FRAME = 1000;
        private const string c_SettingFilePath = @"C:\OurPlant\Logs\CameraSettings\SaveSettings\";
        private const string c_DEFFilePath = @"C:\OurPlant\Logs\CameraSettings\DefaultSettings\";
        private const string c_ConsoleFilePath = @"C:\OurPlant\Logs\CameraSettings\ConsoleOnError\";
        private const string c_DLLPATH = @"C:\Program Files\Allied Vision\Vimba_5.0\VimbaNET\Bin\Win64\VimbaNET.dll";
        /// <summary>
        /// commands
        /// </summary>
        private const string c_SNAPSHOT = "SNAPSHOT";
        private const string c_SETFEATUREVALUES = "SETFEATUREVALUES";
        private const string c_GETFEATUREVALUES = "GETFEATUREVALUES";
        /// <summary>
        /// keys in features
        /// </summary>
        private const string c_GVSPAdjustPacketSize = "GVSPAdjustPacketSize";
        private const string c_GETGVSPPACKETSIZE = "GVSPPacketSize";
        private const string c_StreamBytesPerSecond = "STREAMBYTESPERSECOND";
        /// <summary>
        /// keys in reply
        /// </summary>
        private const string c_OK = "OK";
        private const string c_ERROR = "ERROR";
        private const string c_VIMBA_VERSION = "VIMBAAPI_VERSION";
        private const string c_SERVICEVERSION = "SERVICEVERSION";
        private const string cc_MODEL = "MODEL";
        private const string cc_ADJUSTGVSPPACKETSIZE = "ADJUSTGVSPPACKETSIZE";
        private const string c_Connected_withIP = "Camera connected with";
        private const string c_CAMCONNECTED = "Camera connected";
        private const string c_DISCONNECTED = "Camera disconnected";
        private const string c_NOID_OR_NOTOPEN = "No camera id given or not opened";
        private const string c_NOTOPEN = "Camera not opened";
        private const string c_NO_CAMERA = "NOTFOUND: no cameras found";
        private const string c_NO_CAMERA_ID = "No camera id given";
        private const string c_CONNECTING_ERROR = "Error while connecting camera";
        private const string c_UNKNOWCOMMAND = "unknown command";
        private const string c_EXCEPTION = "EXCEPTION";

        static string[] commands = {
            "HALT",
            "DISCONNECT",
            "CAMERALIST",
            "CONNECTCAM",
            "DISCONNECTCAM",
            c_SNAPSHOT,
            c_GETFEATUREVALUES,
            c_SETFEATUREVALUES,
            "PUSHLASTBYTES",//no function
            "SWVERSION",
            "REVERSEX",
            "REVERSEY",
            "GETGVSPPACKETSIZE",
            "SAVECAMSETTINGSONERROR",
            "LOADCAMSETTINGS",
            "SAVESERVICELOG",
            "LOADDEFAULTSETTINGS"
        };

        static TcpListener Server;
        static bool vServerContinue = true;
        static NetworkStream stream;
        static Dictionary<string, Camera> m_CamsConnected;

        static Dictionary<Camera, Frame> m_CameraFrames = null;
        static ManualResetEvent m_frameReceived = new ManualResetEvent(false);
        static Vimba vimbaLocal = null;
        static int m_CountReceivedFrames = 0;
        static bool fIsSWTrigger;

        public static void Main(string[] args)
        {
            string strExeFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string LastWriteTime = File.GetLastWriteTime(strExeFilePath).ToString(("yyyy.MM.dd hh:mm"));
            Console.WriteLine(AddTimeStamp() + "Starting AlliedVision Camera Service 64 Bit. Last write time: " + LastWriteTime);
            Console.WriteLine(AddTimeStamp() + "Camera Service Version: " + GetVersionFromFileName(System.Reflection.Assembly.GetExecutingAssembly().Location));

            if (!Directory.Exists(c_SettingFilePath))
            {
                DirectoryInfo folder = Directory.CreateDirectory(c_SettingFilePath);
            }
            if (!Directory.Exists(c_DEFFilePath))
            {
                DirectoryInfo folder = Directory.CreateDirectory(c_DEFFilePath);
            }
            if (PriorProcess() != null)
            {
                Console.WriteLine(AddTimeStamp() + "Another instance of the app is already running.");
                return;
            }

            const Int32 port = 13000;
            IPAddress vLocalAdress = IPAddress.Parse("0.0.0.0");
            bool vContinueOneClient = true;

            Server = new TcpListener(vLocalAdress, port);
            Server.Server.NoDelay = true;
            vimbaLocal = new Vimba();
            Console.WriteLine("                         Vimba .NET API Version: " + GetVersionFromFileName(c_DLLPATH));

            vimbaLocal.Startup();
            var bytes = new Byte[256];
            string data = null;
            m_CamsConnected = new Dictionary<string, Camera>();
            m_CameraFrames = new Dictionary<Camera, Frame>();

            Server.Start();
            try
            {
                while (vServerContinue)
                {
                    try
                    {
                        Console.WriteLine(AddTimeStamp() + "waiting for connection at port 13000 ..");
                        TcpClient client = Server.AcceptTcpClient();
                        stream = client.GetStream();
                        vContinueOneClient = true;

                        Console.WriteLine();
                        SendOKResponse(c_Connected_withIP, "", client.Client.LocalEndPoint.ToString() + "  port " + port.ToString());
                        SendOKResponse(c_SERVICEVERSION, "", GetVersionFromFileName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "_" + LastWriteTime);
                        SendOKResponse(c_VIMBA_VERSION, "", GetVersionFromFileName(c_DLLPATH));
                        Console.WriteLine();

                        int i;
                        /// Loop to receive all the data sent by the client.
                        while (vContinueOneClient)
                        {
                            if ((i = stream.Read(bytes, 0, bytes.Length)) == 0)
                                break;
                            /// Translate data bytes to a ASCII string.
                            data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);

                            /// Process the data sent by the client.
                            data = data.Trim().ToUpper();
                            try
                            {
                                vContinueOneClient = CheckCommand(data);
                            }
                            catch (Exception e)
                            {
                                SendERResponse(c_EXCEPTION, "", e.Message);
                            }
                            if (!vContinueOneClient)
                                client.Close();
                        }

                    }
                    catch (Exception e)
                    {
                        SendERResponse(c_EXCEPTION, "", e.Message);
                    }
                }

            }
            finally
            {
                DisconnectAllCams();
                Server.Stop();
                vimbaLocal.Shutdown();
            }
        }

        static void DisconnectAllCams()
        {
            Console.WriteLine(AddTimeStamp() + " DisconnectAllCams");
            foreach (string ID in m_CamsConnected.Keys)
            {
                DisconnectCam(ID);
            }
            m_CamsConnected.Clear();
        }

        static bool CheckCommand(string message)
        {
            var msgSplit = new List<string>();
            string toSend = "";
            string CamID = "";
            string vResult;
            try
            {
                msgSplit.AddRange(message.ToUpper().Split(new char[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries));
                int idx = Array.IndexOf(commands, msgSplit[0]);
                switch (idx)
                {
                    case 0: // halt
                        SendOKResponse(commands[idx], "", "");
                        vServerContinue = false;
                        return false;
                    case 1: // disconnect
                        SendOKResponse(commands[idx], "", "");
                        return false;
                    case 2: // cameralist
                        toSend = buildCamList();
                        if (toSend.Length == 0)
                            SendERResponse(commands[idx], "", c_NO_CAMERA);
                        else
                            SendOKResponse(commands[idx], "", toSend);
                        break;
                    case 3: // CONNECTCAM
                        CamID = GetIDFromMessage(message);
                        if (CamID == "")
                        {
                            SendERResponse(commands[idx], CamID, c_NO_CAMERA_ID);
                            break;
                        }
                        fIsSWTrigger = Convert.ToBoolean(GetTextFromMessage(message, 4));
                        bool IsResetSettings = Convert.ToBoolean(GetTextFromMessage(message, 5));
                        if (ConnectCam(CamID, GetTextFromMessage(message, 2), GetTextFromMessage(message, 3), IsResetSettings))
                            SendOKResponse(commands[idx], CamID, c_CAMCONNECTED);
                        else
                            SendERResponse(commands[idx], CamID, c_CONNECTING_ERROR);
                        break;
                    case 4: // DISCONNECTCAM
                        CamID = GetIDFromMessage(message);
                        DisconnectCam(CamID);
                        break;
                    case 5: // SNAPSHOT
                       // int vTick = Environment.TickCount;
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        if (SendCameraSnapshot(CamID))
                            SendOKResponse(commands[idx], CamID, "");
                       // Console.WriteLine(AddTimeStamp() + "SnapShot: " + (Environment.TickCount - vTick).ToString());
                        break;
                    case 6: // GETFEATUREVALUES
                        CamID = GetIDFromMessage(message);
                        if (m_CamsConnected.ContainsKey(CamID))
                            SendOKResponse(commands[idx], CamID, getFeatureList(message));
                        else
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                        break;
                    case 7: // SETFEATUREVALUES
                        CamID = GetIDFromMessage(message);
                        if (m_CamsConnected.ContainsKey(CamID))
                        {
                            vResult = setFeatures(message);
                            if (vResult.Contains(c_EXCEPTION))
                            {
                                SendERResponse(commands[idx], CamID, vResult);
                            }
                            else
                                SendOKResponse(commands[idx], CamID, message);
                        }
                        else
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                        break;
                    case 8: //pushlastbytes
                        break;
                    case 9://SWVERSION
                        SendOKResponse(commands[idx], "", GetVersionFromFileName(System.Reflection.Assembly.GetExecutingAssembly().Location));
                        break;
                    case 10://REVERSEX
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        SendOKResponse(commands[idx], "", "");
                        break;
                    case 11://REVERSEY
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        SendOKResponse(commands[idx], "", "");
                        break;
                    case 12://GETGVSPPACKETSIZE
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        if (GetGVSPPacketSize(CamID, out vResult))
                            SendOKResponse(commands[idx], CamID, vResult);
                        else
                            SendERResponse(commands[idx], CamID, "");
                        break;
                    case 13://SAVECAMSETTINGSONERROR
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        if (SaveCAMSettingsOnError(GetCamera(CamID), GetTextFromMessage(message, 2)))
                            SendOKResponse(commands[idx], CamID, "");
                        break;
                    case 14://LOADCAMSETTINGS
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        if (LoadCAMSettingsFromFile(CamID, GetTextFromMessage(message, 2)))
                            SendOKResponse(commands[idx], CamID, "");
                        else
                            SendERResponse(commands[idx], CamID, "");
                        break;
                    case 15://SAVESEVICELOG to be continued
                            //SaveServiceLogOnError; 
                        break;
                    case 16://LOADDEFAULTSETTINGS
                        CamID = GetIDFromMessage(message);
                        if (!m_CamsConnected.ContainsKey(CamID))
                        {
                            SendERResponse(commands[idx], CamID, c_NOID_OR_NOTOPEN);
                            break;
                        }
                        if (LoadCAMSettingsFromFile(CamID, GetDefaultFilePath(CamID)))
                            SendOKResponse(commands[idx], CamID, "");
                        else
                            SendERResponse(commands[idx], CamID, "");
                        break;
                    default:
                        SendERResponse(c_UNKNOWCOMMAND, "", message);
                        break;
                }
            }
            catch (Exception e)
            {
                SendERResponse(c_EXCEPTION, CamID, " CheckCommand" + e.Message);
            }
            return true;
        }

        static string setFeatures(string msg)  // SETFEATUREVALUES
        {
            string result = "";
            string featureName;
            string value = "";
            string[] split = msg.Split(new char[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries);
            string CamID = split[1];
            result = CamID;
            for (int i = 2; i <= split.Length - 1; i++)
            {
                featureName = split[i].Split(new char[] { '=' })[0];
                foreach (Feature feature in m_CamsConnected[CamID].Features)
                {
                    if (feature.Name.ToUpper() == featureName.ToUpper())
                    {
                        try
                        {
                            value = split[i].Split(new char[] { '=' })[1];
                            switch (feature.DataType)
                            {
                                case VmbFeatureDataType.VmbFeatureDataBool:
                                    feature.BoolValue = Convert.ToBoolean(value);
                                    break;
                                case VmbFeatureDataType.VmbFeatureDataEnum:
                                    feature.EnumValue = value;
                                    break;
                                case VmbFeatureDataType.VmbFeatureDataFloat:
                                    feature.FloatValue = Convert.ToDouble(value);
                                    break;
                                case VmbFeatureDataType.VmbFeatureDataInt:
                                    feature.IntValue = Str2Int(value, 10);
                                    break;
                                case VmbFeatureDataType.VmbFeatureDataString:
                                    feature.StringValue = value;
                                    break;
                                case VmbFeatureDataType.VmbFeatureDataCommand:
                                    feature.RunCommand();
                                    break;
                                default:
                                    result = c_EXCEPTION + " " + feature.Name + " " + "Unknown Tpye" + feature.DataType.ToString();
                                    break;
                            }
                            result = feature.Name + "=" + value;
                        }
                        catch (Exception exception)
                        {
                            result = c_EXCEPTION + " " + feature.Name + " " + value + " " + exception.Message;
                        }
                        break;
                    }
                }
            }
            return result;
        }

        static string getFeatureList(string msg) // GETFEATUREVALUES
        {
            string result = "";
            object value;
            string[] split = msg.Split(new char[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries);
            string CamID = split[1];
            result = CamID;
            try
            {
                for (int i = 2; i <= split.Length - 1; i++)
                {
                    foreach (Feature feature in m_CamsConnected[CamID].Features)
                    {
                        if (feature.Name.ToUpper() == split[i].ToUpper())
                        {
                            switch (feature.DataType)
                            {
                                case VmbFeatureDataType.VmbFeatureDataBool:
                                    value = feature.BoolValue;
                                    break;
                                case VmbFeatureDataType.VmbFeatureDataCommand:
                                    value = null;
                                    break;
                                case VmbFeatureDataType.VmbFeatureDataEnum:
                                    value = feature.EnumValue;
                                    break;
                                case VmbFeatureDataType.VmbFeatureDataFloat:
                                    value = feature.FloatValue;
                                    break;
                                case VmbFeatureDataType.VmbFeatureDataInt:
                                    value = feature.IntValue;
                                    break;
                                case VmbFeatureDataType.VmbFeatureDataString:
                                    value = feature.StringValue;
                                    break;
                                default:
                                    value = null;
                                    break;
                            }
                            result = result + "|" + feature.Name + "=" + value.ToString();
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                SendERResponse(c_EXCEPTION, "", e.Message);
            }
            return result;
        }

        static string buildCamList()
        {
            string result = "";
            Console.Write(AddTimeStamp() + "   Cameras found:");
            foreach (Camera camera in vimbaLocal.Cameras)
            {
                result = result + camera.Id + ",";
                Console.WriteLine();
                Console.Write("                            " + camera.Id);
            }
            Console.WriteLine();
            return result.TrimEnd(new char[] { ',' });
        }

        static void SendOKResponse(string msg, string ID, string Text)
        {
            msg = c_OK + "|" + msg + "|" + ID + "|" + Text;
            SendString(msg);
            if (!msg.Contains(c_SNAPSHOT))
            Console.WriteLine(AddTimeStamp() + msg);
        }

        static void SendERResponse(string msg, string ID, string Text)
        {
            msg = c_ERROR + "|" + msg + "|" + ID + "|" + Text;
            SendString(msg);
            Console.WriteLine(AddTimeStamp() + msg);
        }

        static string GetDefaultFilePath(string CamID)
        {
            string Result = c_DEFFilePath + CamID + "_Default.xml";
            return Result;
        }

        static int Str2Int(string Str, int DefValue)
        {
            int result;
            try
            {
                result = int.Parse(Str, NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite);
            }
            catch
            {
                result = DefValue;
            }
            return result;
        }

        static bool SaveCAMSettingsOnOpen(Camera camera, bool SaveFile, bool SetDefault, string filePath)
        {
            bool result = false;
            try
            {
                if (SetDefault | SaveFile)
                {
                    camera.Open(VmbAccessModeType.VmbAccessModeFull);
                    string cameraId = camera.Id;
                    string fileName = cameraId + "_" + DateTime.Now.ToString("yyyyddMM_HHmmss") + ".xml";
                    camera.LoadSaveSettingsSetup(VmbFeaturePersistType.VmbFeaturePersistNoLUT, 2, 4);
                    if (SaveFile)
                    {
                        camera.SaveCameraSettings(filePath + fileName);
                        Console.WriteLine("--> Camera settings have been saved to " + fileName);
                    }
                    if (SetDefault)//  restore factory settings by loading user default set
                    {
                        Feature feature = camera.Features["UserSetSelector"];
                        feature.EnumValue = "Default";
                        feature = camera.Features["UserSetLoad"];
                        feature.RunCommand();
                        if (!File.Exists(GetDefaultFilePath(cameraId)))
                            camera.SaveCameraSettings(GetDefaultFilePath(cameraId));
                    }
                    camera.Close();
                    result = true;
                }
                else
                    result = true;
            }
            catch (Exception exception)
            {
                result = false;
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return result;
        }

        static void SaveServiceLogOnError()
        {
            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;

            if (!Directory.Exists(c_ConsoleFilePath))
            {
                DirectoryInfo folder = Directory.CreateDirectory(c_ConsoleFilePath);
            }
            string fileName = "ConsoleOnError" + DateTime.Now.ToString("yyyyddMM_HHmmss") + ".txt";
            try
            {
                ostrm = new FileStream(c_ConsoleFilePath + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open" + c_ConsoleFilePath + fileName);
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(writer);
            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
            Console.WriteLine("Done");
        }

        static bool SaveCAMSettingsOnError(Camera camera, string FileName)
        {
            bool result = false;
            string Name, vResult;
            try
            {
                camera.Close();
                camera.Open(VmbAccessModeType.VmbAccessModeFull);
                camera.LoadSaveSettingsSetup(VmbFeaturePersistType.VmbFeaturePersistNoLUT, 2, 4);
                Name = FileName.Replace(".", " ");
                Name = Name.Replace("XML", "");
                Name = Name + ".xml";
                camera.SaveCameraSettings(Name);
                vResult = Path.GetFileName(Name);
                Console.WriteLine(AddTimeStamp() + "    SaveCAMSettingsOnError: " + vResult);
                camera.Close();
                result = true;
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return result;
        }

        static bool GetGVSPPacketSize(string ID, out string size)
        {
            bool result = false;
            size = "0";
            try
            {
                foreach (Camera camera in vimbaLocal.Cameras)
                {
                    if (camera.Id == ID)
                    {
                        size = camera.Features[c_GETGVSPPACKETSIZE].IntValue.ToString();
                        result = true;
                        break;
                    }
                }
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return result;
        }

        static string GetTextFromMessage(string message, int Idx)
        {
            try
            {
                string[] split = message.Split(new char[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length > Idx)
                    return split[Idx];
                else
                    return "";
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
                return "";
            }
        }

        static string GetIDFromMessage(string message)
        {
            try
            {
                string[] split = message.Split(new char[] { '<', '|', '>' }, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length > 1)
                    return split[1];
                else
                    return "";
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
                return "";
            }
        }

        static bool SendCameraSnapshotV1(string ID)
        {
            var tmpMsg = new List<byte>();
            Frame frame = null;
            string message = "FRAME";
            try
            {
                int vTick = Environment.TickCount;
                m_CamsConnected[ID].AcquireSingleImage(ref frame, cTIMEOUT_FRAME);
                if ((Environment.TickCount - vTick) > 200)
                {
                    Console.WriteLine(AddTimeStamp() + "Response time aquiring image for " + ID + ": " + (Environment.TickCount - vTick).ToString());
                }
                message = Concatenate(message, ID);
                message = Concatenate(message, frame.Buffer.Length.ToString());
                message = Concatenate(message, frame.Width.ToString());
                message = Concatenate(message, frame.Height.ToString());
                message = "<" + message + ">";
                tmpMsg.AddRange(Encoding.ASCII.GetBytes(message));
                tmpMsg.AddRange(frame.Buffer);

                vTick = Environment.TickCount;
                stream.Write(tmpMsg.ToArray(), 0, tmpMsg.Count);
                if (Environment.TickCount - vTick > 200)
                {
                    Console.WriteLine(AddTimeStamp() + "Delay of sending image of {0}: {1}", ID, (Environment.TickCount - vTick).ToString());
                }
                if ((frame.Width * frame.Height == 0) & m_CamsConnected[ID].Features.ContainsName("GVSPAdjustPacketSize"))
                {
                    m_CamsConnected[ID].Features["GVSPAdjustPacketSize"].RunCommand();
                    Console.WriteLine(AddTimeStamp() + "adjusting PacketSize for " + ID);
                }
            }
            catch (Exception e)
            {
                SendERResponse(c_SNAPSHOT, ID, c_EXCEPTION + e.Message);
                return false;
            }
            return true;
        }

        static bool SendCameraSnapshot(string ID)
        {
            bool result = false;
            if (fIsSWTrigger)
            {
                result = SendCameraSnapshotV2(ID);
            }
            else
            {
                result = SendCameraSnapshotV1(ID);
            }
            return result;
        }

        static bool ConnectCam(string ID, string IsSaveSettings, string FilePath, bool IsResetSettings)
        {
            bool result = false;
            if (m_CamsConnected.ContainsKey(ID))
                DisconnectCam(ID);

            if (fIsSWTrigger)
            {
                result = ConnectCamV2(ID, IsSaveSettings, FilePath, IsResetSettings);
            }
            else
            {
                result = ConnectCamV1(ID, IsSaveSettings, FilePath, IsResetSettings);
            }
            return result;
        }

        static bool ConnectCamV1(string ID, string IsSaveSettings, string FilePath, bool IsResetSettings)
        {
            bool result = false;
            try
            {
                foreach (Camera camera in vimbaLocal.Cameras)
                {
                    if (camera.Id == ID)
                    {
                        Console.WriteLine(AddTimeStamp() + "---------------------------------------------------------AcquireSingleImage mode--->connect " + ID);
                        VmbAccessModeType accessMode = camera.PermittedAccess;
                        if (VmbAccessModeType.VmbAccessModeFull == (VmbAccessModeType.VmbAccessModeFull & accessMode))
                        {
                            SaveCAMSettingsOnOpen(camera, IsSaveSettings == "1", IsResetSettings, c_SettingFilePath);

                            camera.Open(VmbAccessModeType.VmbAccessModeFull);

                            SendOKResponse(cc_MODEL, ID, camera.Model);
                            if (!m_CamsConnected.ContainsKey(ID))
                                m_CamsConnected.Add(ID, camera);

                            AdjustPacketSize(ID);
                            GetStreamBytesPerSecond(ID);
                            result = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return result;
        }

        static void DisconnectCam(string ID)
        {
            if (!m_CamsConnected.ContainsKey(ID))
                return;
            if (fIsSWTrigger)
            {
                DisconnectCamV2(ID);
            }
            else
            {
                DisconnectCamV1(ID);
            }
        }

        static void DisconnectCamV1(string ID)
        {
            try
            {
                m_CamsConnected[ID].Close();
                SendOKResponse("DISCONNECTCAMV1", ID, c_DISCONNECTED);
            }
            finally
            {
                m_CamsConnected.Remove(ID);
            }
        }

        static bool ConnectCamV2(string ID, string IsSaveSettings, string FilePath, bool IsResetSettings)
        {
            bool result = false;
            long m_payloadsize = 0;

            foreach (Camera camera in vimbaLocal.Cameras)
            {
                if (camera.Id == ID)
                {
                    Console.WriteLine(AddTimeStamp() + "---------------------------------------------------------------SW Trigger mode--->connect " + ID);
                    VmbAccessModeType accessMode = camera.PermittedAccess;
                    if (VmbAccessModeType.VmbAccessModeFull == (VmbAccessModeType.VmbAccessModeFull & accessMode))
                    {
                        SaveCAMSettingsOnOpen(camera, IsSaveSettings == "1", IsResetSettings, c_SettingFilePath);
                        camera.Open(VmbAccessModeType.VmbAccessModeFull);
                        try
                        {
                            camera.Features["TriggerSelector"].EnumValue = "FrameStart";
                        }
                        catch (VimbaException ve)
                        {
                            SendERResponse(c_EXCEPTION, "", ve.Message + " TriggerSelector FrameStart");
                        }
                        try
                        {
                            camera.Features["TriggerSource"].StringValue = "Software";
                        }
                        catch (VimbaException ve)
                        {
                            SendERResponse(c_EXCEPTION, "", ve.Message + " TriggerSource Software");
                        }
                        try
                        {
                            camera.Features["TriggerMode"].StringValue = "On";
                            SendOKResponse(cc_MODEL, ID, camera.Model);
                            if (!m_CamsConnected.ContainsKey(camera.Id))
                                m_CamsConnected.Add(camera.Id, camera);
                        }
                        catch (VimbaException ve)
                        {
                            SendERResponse(c_EXCEPTION, "", ve.Message + " TriggerMode On");
                        }
                        try
                        {
                            AdjustPacketSize(camera.Id);

                            m_payloadsize = camera.Features["PayloadSize"].IntValue;//payLoadSize = width*hight of frame
                            Frame frame = new Frame(m_payloadsize);
                            if (!m_CameraFrames.ContainsKey(camera))
                                m_CameraFrames.Add(camera, frame);

                            camera.AnnounceFrame(frame);
                            camera.StartCapture();
                            camera.QueueFrame(frame);
                            camera.OnFrameReceived += doOnFrameReceived;

                            camera.Features["AcquisitionStart"].RunCommand();
                            result = true;
                        }
                        catch (Exception exception)
                        {
                            SendERResponse(c_EXCEPTION, "", exception.Message);
                        }
                        GetStreamBytesPerSecond(ID);
                        break;

                    }
                }
            }
            return result;
        }
        static void AdjustPacketSize(string ID)
        {
            if (!m_CamsConnected.ContainsKey(ID))
                SendERResponse(cc_ADJUSTGVSPPACKETSIZE, ID, c_NOID_OR_NOTOPEN);
            try
            {
                foreach (Camera camera in vimbaLocal.Cameras)
                {
                    if (camera.Id == ID)
                    {
                        /// set GVSP packet size to max value (MTU) and wait until command is done
                        camera.Features[c_GVSPAdjustPacketSize].RunCommand();

                        /// check if operation is done
                        bool lFlag = false;
                        do
                        {
                            lFlag = camera.Features[c_GVSPAdjustPacketSize].IsCommandDone();
                        } while (false == lFlag);

                        string result;
                        result = camera.Features[c_GETGVSPPACKETSIZE].IntValue.ToString();
                        SendOKResponse(cc_ADJUSTGVSPPACKETSIZE, ID, result);//"GVSP packet size has been set to maximum "
                        break;
                    }
                }
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, ID, exception.Message);
            }
        }

        static void DisconnectCamV2(string ID)
        {
            try
            {
                Camera camera = m_CamsConnected[ID];
                try
                {
                    camera.OnFrameReceived -= doOnFrameReceived;
                    camera.Features["AcquisitionStop"].RunCommand();
                    camera.EndCapture();
                    camera.FlushQueue();
                    camera.RevokeAllFrames();
                    camera.Features["TriggerSource"].StringValue = "Freerun";
                    // camera.Features["TriggerMode"].StringValue = "Off";

                    camera.Close();
                }
                finally
                {
                    m_CameraFrames.Remove(camera);
                }
            }
            finally
            {
                m_CamsConnected.Remove(ID);
                SendOKResponse("DISCONNECTCAMV2", ID, c_DISCONNECTED);
            }
        }

        static bool SendCameraSnapshotV2(string ID)
        {
            long m_payloadsize = 0;
            Camera camera = m_CamsConnected[ID];
            Frame frame;
            int tries = 1;
            int vTick;

            if (!m_CameraFrames.ContainsKey(camera))
            {
                try
                {
                    m_payloadsize = camera.Features["PayloadSize"].IntValue;//payLoadSize = width*hight of frame
                    frame = new Frame(m_payloadsize);
                    m_CameraFrames.Add(camera, frame);
                    Console.WriteLine(AddTimeStamp() + "new frame " + ID);
                }
                catch (Exception e)
                {
                    SendERResponse(c_SNAPSHOT, ID, c_EXCEPTION + " PayloadSize" + e.Message);
                    return false;
                }
            }
            else
            {
                frame = m_CameraFrames[camera];
            }
            ///reset frames
            try
            {
                if (m_CountReceivedFrames > 1)
                    camera.QueueFrame(frame); // in case we received a frame before aborting aquisition
                m_CountReceivedFrames = 0;
            }
            catch (Exception e)
            {
                SendERResponse(c_SNAPSHOT, ID, c_EXCEPTION + " reset frames " + e.Message);
                return false;
            }
            try
            {
                vTick = Environment.TickCount;
                do
                {
                    m_frameReceived.Reset();
                    camera.Features["TriggerSoftware"].RunCommand();
                    if (m_frameReceived.WaitOne(cTIMEOUT_FRAME + 90) && (frame.ReceiveStatus == VmbFrameStatusType.VmbFrameStatusComplete))
                        break;
                } while (++tries < 5);

                ///abort if not received
                if (!m_frameReceived.WaitOne(0))
                {
                    // camera.Features["AcquisitionAbort"].RunCommand();
                    SendERResponse(c_SNAPSHOT, ID, "frame not received after " + (Environment.TickCount - vTick).ToString());
                }
                if (tries > 1)
                {
                    Console.WriteLine(AddTimeStamp() + " {0} intents needed to acquire image from camera {1}", tries, ID);
                }
                if ((Environment.TickCount - vTick) > 200)
                {
                    Console.WriteLine(AddTimeStamp() + "Response time aquiring image for {0}: {1}ms", ID, (Environment.TickCount - vTick).ToString());
                }
            }
            catch (Exception e)
            {
                SendERResponse(c_SNAPSHOT, ID, c_EXCEPTION + " receive frame" + e.Message);
                return false;
            }
            ///send frame to OPOS
            try
            {
                vTick = Environment.TickCount;
                string message = "<FRAME" + "|" + ID + "|" + frame.Buffer.Length.ToString() + "|" + frame.Width.ToString() + "|" + frame.Height.ToString() + ">";

                var byteMsg = Encoding.ASCII.GetBytes(message);
                stream.Write(byteMsg, 0, byteMsg.Length);
                stream.Write(frame.Buffer, 0, frame.Buffer.Length);
                if (Environment.TickCount - vTick > 200)
                {
                    Console.WriteLine(AddTimeStamp() + "Delay of sending image of {0}: {1}", ID, (Environment.TickCount - vTick).ToString());
                }
            }
            catch (Exception e)
            {
                SendERResponse(c_SNAPSHOT, ID, c_EXCEPTION + " send frame to OPOS" + e.Message);
                return false;
            }
            /// prepare for next frame
            try
            {
                if (tries > 1)
                    camera.FlushQueue();
                camera.QueueFrame(frame);
            }
            catch (Exception e)
            {
                SendERResponse(c_SNAPSHOT, ID, c_EXCEPTION + " prepare next frame" + e.Message);
                return false;
            }
            return true;
        }
        static void GetStreamBytesPerSecond(string ID)
        {
            try
            {
                SendOKResponse(c_GETFEATUREVALUES, ID, getFeatureList("<" + c_GETFEATUREVALUES + "|" + ID + "|" + c_StreamBytesPerSecond + ">"));
            }
            catch (Exception e)
            {
                SendERResponse(c_EXCEPTION, ID, e.Message + c_GETFEATUREVALUES);
            }
        }

        static void doOnFrameReceived(Frame frame)
        {
            m_frameReceived.Set();
            if (++m_CountReceivedFrames > 1)
                Console.WriteLine(AddTimeStamp() + "count received frames: " + m_CountReceivedFrames.ToString());
            try
            {
                int vTick = Environment.TickCount;
                while ((frame.ReceiveStatus != VmbFrameStatusType.VmbFrameStatusComplete) & ((Environment.TickCount - vTick) < cTIMEOUT_FRAME))
                {
                    System.Threading.Thread.Sleep(5);
                };

                if (frame.ReceiveStatus != VmbFrameStatusType.VmbFrameStatusComplete)
                    Console.WriteLine(AddTimeStamp() + "doOnFrameReceived-->frame status not complete: " + frame.ReceiveStatus.ToString() + " " + (Environment.TickCount - vTick).ToString());
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
        }

        static void SendString(string messageToSend)
        {
            messageToSend = "<" + messageToSend + ">";
            byte[] msg = Encoding.ASCII.GetBytes(messageToSend);
            stream.Write(msg, 0, msg.Length);

        }

        static string Concatenate(string part1, string part2)
        {
            return part1 + '|' + part2;
        }

        static string AddTimeStamp()
        {
            DateTime now = DateTime.Now; // <-- Value is copied into local
            return now.ToString("yyyy.MM.dd hh:mm:ss:fff") + ": ";
        }

        static bool LoadCAMSettingsFromFile(string ID, string FileName)
        {
            bool result = false;
            try
            {
                foreach (Camera camera in vimbaLocal.Cameras)
                {
                    if (camera.Id == ID)
                    {
                        VmbAccessModeType accessMode = camera.PermittedAccess;
                        if (VmbAccessModeType.VmbAccessModeFull == (VmbAccessModeType.VmbAccessModeFull & accessMode))
                        {
                            camera.Open(VmbAccessModeType.VmbAccessModeFull);
                            string cameraId = camera.Id;
                            Feature feature = camera.Features["UserSetSelector"];
                            feature.EnumValue = "Default";
                            feature = camera.Features["UserSetLoad"];
                            feature.RunCommand();
                            camera.LoadCameraSettings(FileName);
                            Console.WriteLine("--> Feature values have been loaded from '" + FileName + "'");
                            camera.Close();
                            result = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return result;
        }

        static Camera GetCamera(string ID)
        {
            Camera result = null;
            try
            {
                foreach (Camera camera in vimbaLocal.Cameras)
                {
                    if (camera.Id == ID)
                    {
                        result = camera;
                        break;
                    }
                }
            }
            catch (Exception exception)
            {
                result = null;
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return result;
        }
        
        public static String GetVersionFromFileName(string ProcessName)
        {
            string ResultDev = "";
            try
            {
                if (!File.Exists(ProcessName))
                    return "File not found";
                FileVersionInfo vFileVersionInfo = FileVersionInfo.GetVersionInfo(ProcessName);
                string vVerPrd = vFileVersionInfo.ProductVersion;
                if (vVerPrd == "0.0.0.0")
                {
                    ResultDev = "\n                         Developer Version"
                        + "\n                         LastUpdate: " + GitConstants.LASTUPDATE
                        + "\n                         Based on: "   + GitConstants.GIT_LAST_TAG
                        + "\n                         Short Hash: " + GitConstants.GIT_LAST_HASH
                        + "\n                         Branch: "     + GitConstants.GIT_CURRENT_BRANCH
                        + "\n                         Author: "     + GitConstants.GIT_AUTHOR_NAME
                        + "\n                         Commit: "     + GitConstants.GIT_COMMIT_DATE;
                    return ResultDev;
                }
                else
                    return vVerPrd;
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return "Exception: cannot extract version number";
        }

        public static Process PriorProcess()
        // Returns a System.Diagnostics.Process pointing to
        // a pre-existing process with the same name as the
        // current one, if any; or null if the current process
        // is unique.
        {
            try
            {
                Process curr = Process.GetCurrentProcess();
                Process[] procs = Process.GetProcessesByName(curr.ProcessName);
                foreach (Process p in procs)
                {
                    if ((p.Id != curr.Id) &&
                        (p.MainModule.FileName == curr.MainModule.FileName))
                        return p;
                }
            }
            catch (Exception exception)
            {
                SendERResponse(c_EXCEPTION, "", exception.Message);
            }
            return null;
        }
    }
}